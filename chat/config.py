import os
from chat.utils import BaseDevConfig, BaseServiceConfig, \
    BaseTestConfig, BaseConfig


class DevelopmentConfig(BaseDevConfig, BaseServiceConfig):
    SQLALCHEMY_DATABASE_URI=BaseDevConfig.SQLALCHEMY_DATABASE_URI


class TestingConfig(BaseTestConfig, BaseServiceConfig):
    SQLALCHEMY_DATABASE_URI=BaseTestConfig.SQLALCHEMY_DATABASE_URI


class ProductionConfig(BaseConfig, BaseServiceConfig):
    SQLALCHEMY_DATABASE_URI=BaseConfig.SQLALCHEMY_DATABASE_URI