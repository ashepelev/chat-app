import os

from chat.app_factory import create_app

app_config = os.getenv('APP_SETTINGS', 'chat.config.DevelopmentConfig')
app = create_app(app_config)
